const stalker = document.getElementById('stalker'); 
document.addEventListener('mousemove', function (e) {
    stalker.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
});

let x = document.getElementById('login');
let y = document.getElementById('register');
let z = document.getElementById('btn');

function register() {
    x.style.left = '-400px';
    y.style.left = '50px';
    z.style.left = '110px';
}
function login() {
    x.style.left = '50px';
    y.style.left = '450px';
    z.style.left = '0';
}

$(function(){
    // fadein
    $(window).scroll(function (){
        $('.fadein').each(function(){
            let targetElement = $(this).offset().top;
            let scroll = $(window).scrollTop();
            let windowHeight = $(window).height();
            if (scroll > targetElement - windowHeight + 200){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            } else {
                $(this).css('opacity','0');
                $(this).css('transform','translateY(-100%)');
            }
        });
    });

    // ページTOPに戻る
    let topBtn = $('#page-top');    
    topBtn.hide();

    // スクロールダウン文言
    let scrollDown = $('.scroll-down');
    scrollDown.show();
    
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
            scrollDown.fadeOut();
        } else {
            topBtn.fadeOut();
            scrollDown.fadeIn();
        }
    });

    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    // スクロールするとヘッダーが切り替わる
    let menuHeight = $(".menu-wrap").height();
    let startPos = 0;
    $(".menu-wrap").css("top", "-" + menuHeight + "px");
    $(window).scroll(function(){
        let currentPos = $(this).scrollTop();
        if (currentPos > startPos) {
            if($(window).scrollTop() >= 380) {
                $(".header ul li a").css("color", "#000");
                $(".menu-wrap").css("top", 0 + "px");
            }
        } else {
            $(".header ul li a").css("color", "#fff");
            $(".menu-wrap").css("top", "-" + menuHeight + "px");
        }
        startPos = currentPos;
    });

    // slickスライダー
    $('.single').slick({
        autoplaySpeed: 3000,
        autoplay: true,
        arrows: false,
    });

    // モーダル
    $('.post-add-modal-open').on('click',function(){
        $('.post-add-modal').fadeIn();
        return false;
    });
    // モーダルクローズ
    $('.post-add-modal-close').on('click',function(){
        $('.post-add-modal').fadeOut();
        return false;
    });
    // モーダル内クローズアイコン
    $('.post-add-modal-close-icon').on('click',function(){
        $('.post-add-modal').fadeOut();
        return false;
    });

});