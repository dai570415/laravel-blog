@extends('layouts.app')
@section('title', 'DESIGN BLOG')

@section('content')
    <div class="contents-heading">
        DETAIL<span>記事</span>
    </div>

    @if($authUser)

        @if($authUser->id == $item->user_id)
            <div class="post-add">
                <a class="post-add-modal-open" href="">記事を編集する</a>
            </div>
        @endif

        @if($authUser->id == $item->user_id)
        <div class="modal post-add-modal">
        <div class="modal__bg post-add-modal-close"></div>
            <div class="modal__content">
                <img src="{{ asset('img/closeIcon_black.svg') }}" class="post-add-modal-close-icon">
                <form action="/post/{{$item->id}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}           
                    <input type="hidden" name="user_id" value="{{ $authUser->id }}">
                    <input type="text" class="post-input" name="title" placeholder="タイトル" value="{{ $item->title }}">
                    <div>
                        <textarea class="post-textarea" name="message" placeholder="メッセージ">{{ $item->message }}</textarea>
                    </div>
                    <select name="category" class="post-select">
                        <option value="ノウハウ" <?= $item->category == 'ノウハウ' ? 'selected' : '' ?> >ノウハウ</option>
                        <option value="やってみた" <?= $item->category == 'やってみた' ? 'selected' : '' ?> >やってみた</option>
                        <option value="おすすめ"  <?= $item->category == 'おすすめ' ? 'selected' : '' ?> >おすすめ</option>
                        <option value="レポート" <?= $item->category == 'レポート' ? 'selected' : '' ?> >レポート</option>
                        <option value="広告" <?= $item->category == '広告' ? 'selected' : '' ?> >広告</option>
                    </select>

                    <input type="file" name="image">

                    <input type="hidden" name="_method" value="PUT">
                    <input type="submit" class="post-create post-edit" value="記事を編集する">
                </form>
            </div>
        </div>
        @endif
    @endif
    記事詳細をここに描画（認証なしで閲覧可）<br />
    <a href="{{ route('post.index') }}">&gt; 前に戻る</a>
@endsection

{{--<!-- @if($item->image)
    <img src="/storage/post/{{ $item->image }}" class="image" style="width:20%;">
@endif -->--}}