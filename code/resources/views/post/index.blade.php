@extends('layouts.app')
@section('title', 'DESIGN BLOG')

@section('content')
    <div class="contents-heading">
        PICK UP<span>注目の記事</span>
    </div>
    
    @if($authUser)
        <div class="post-add">
            <a class="post-add-modal-open" href="">記事を投稿する</a>
        </div>
    @endif

    <!-- 4列パターン -->
    <div class="content-item-4">
        @foreach($pickupItems as $pickupItem)
        <div class="content-item">
            @if($pickupItem->category == 'ノウハウ')
            <div class="content-category coral-back">{{ $pickupItem->category }}</div>
            @endif
            @if($pickupItem->category == 'やってみた')
            <div class="content-category cornflowerblue-back">{{ $pickupItem->category }}</div>
            @endif
            @if($pickupItem->category == 'おすすめ')
            <div class="content-category lightseagreen-back">{{ $pickupItem->category }}</div>
            @endif
            @if($pickupItem->category == 'レポート')
            <div class="content-category gold-back">{{ $pickupItem->category }}</div>
            @endif
            @if($pickupItem->category == '広告')
            <div class="content-category indianred-back">{{ $pickupItem->category }}</div>
            @endif
            <a href="/post/{{ $pickupItem->id }}">
                <img src="/storage/post/{{ $pickupItem->image }}">
                <div class="content-item-title">
                    {{ mb_substr($pickupItem->title, 0, 14) }}...
                </div>
            </a>
        </div>
        @endforeach
    </div><!-- 4列パターン -->

    <!-- 3列パターン -->
    <div class="contents-heading">
        NEW POST<span>最新の記事</span>
    </div>

    <div class="content-item-3">
        @foreach($items as $item)
        <div class="content-item fadein">
            @if($item->category == 'ノウハウ')
            <div class="content-category coral-back">{{ $item->category }}</div>
            @endif
            @if($item->category == 'やってみた')
            <div class="content-category cornflowerblue-back">{{ $item->category }}</div>
            @endif
            @if($item->category == 'おすすめ')
            <div class="content-category lightseagreen-back">{{ $item->category }}</div>
            @endif
            @if($item->category == 'レポート')
            <div class="content-category gold-back">{{ $item->category }}</div>
            @endif
            @if($item->category == '広告')
            <div class="content-category indianred-back">{{ $item->category }}</div>
            @endif

            <a href="/post/{{ $item->id }}">
                <img src="/storage/post/{{ $item->image }}" alt="photo1">
                <div class="content-item-title">
                {{ mb_substr($item->title, 0, 18) }}...
                </div>
            </a>
            
            <div class="content-item-info">
                <div class="user-info">
                    @foreach($users as $user)
                        @if($user->id == $item->user_id)
                            @if($user->thumbnail)
                            <img src="/storage/user/{{ $user->thumbnail }}" alt="thumbnail">
                            @else
                            <div class="no-thumbnail"></div>
                            @endif
                            <span>{{ $user->name }}</span>
                        @endif
                    @endforeach
                </div>
                
                <span class="create-date">{{ mb_substr($item->created_at, 0, 10) }}</span>
            </div>
        </div>
        @endforeach
    </div><!-- 3列パターン -->

<!-- Add API -->
@if($authUser)
    <!-- 投稿フォーム -->
    <div class="modal post-add-modal">
    <div class="modal__bg post-add-modal-close"></div>
        <div class="modal__content">
            <img src="{{ asset('img/closeIcon_black.svg') }}" class="post-add-modal-close-icon">
            <form action="/post" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <input type="hidden" name="user_id" value="{{ $authUser->id }}">
                <input type="text" class="post-input" name="title" placeholder="タイトル" value="{{ old('title') }}">
                @if($errors->has('title'))
                    <div class="error">{{ $errors->first('title') }}</div>
                @endif
                <div>
                    <textarea class="post-textarea" name="message" placeholder="メッセージ">{{ old('message') }}</textarea>
                </div>
                @if($errors->has('message'))
                    <div class="error">{{ $errors->first('message') }}</div>
                @endif
                <select name="category" class="post-select">
                    <option value="ノウハウ">ノウハウ</option>
                    <option value="やってみた">やってみた</option>
                    <option value="おすすめ">おすすめ</option>
                    <option value="レポート">レポート</option>
                    <option value="広告">広告</option>
                </select>

                <input type="file" name="image">

                <input type="submit" class="post-create" value="記事を投稿する">
            </form>
        </div>
    </div>

@endif
<!-- Add API -->

@endsection