<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/slick.js') }}"></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="./js/slick.js" type="text/javascript"></script>
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container">

            <div class="header menu-wrap">
                <div class="logo"><a href="">DESIGN BLOG'S</a></div>
                <ul>
                @if(!$authUser)
                    <li><a href="/">ログイン / 無料登録</a></li>
                @else
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            ログアウト
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endif
                </ul>
            </div>
            
            <div class="main">
                <div class="main-title">
                    DESIGN BLOG'S<span>- クリエイターのための情報発信ブログ -</span>
                </div>
                <div class="single">
                    <div class="image" style="background-image: linear-gradient( 135deg, rgba(255, 160, 0, 0.5),rgba(61, 34, 2, 0.5)),url({{ asset('./img/ph1.jpg') }});"></div>
                    <div class="image" style="background-image: linear-gradient( 135deg, rgba(255, 105, 180, 0.8),rgba(73, 36, 48, 0.5)),url({{ asset('./img/ph2.jpg') }});"></div>
                    <div class="image" style="background-image: linear-gradient( 135deg, rgba(102, 205, 170, 0.5), rgba(34, 139, 34, 0.5)),url({{ asset('./img/ph4.jpg') }});"></div>
                    <div class="image" style="background-image: linear-gradient( 135deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.8)),url({{ asset('./img/ph3.jpg') }});"></div>
                </div>
                <ol>
                    <li><a href="">ノウハウ</a></li>
                    <li><a href="">やってみた</a></li>
                    <li><a href="">おすすめ</a></li>
                    <li><a href="">レポート</a></li>
                    <li><a href="">PR</a></li>
                </ol>
            </div>

            <div class="contents">
                @yield('content')
            </div>

            <div class="footer">
                <div class="footer-contents">
                    <div class="footer-content">
                        <div class="explanation">
                            <div class="footer-content-title">SampleSiteとは</div>
                            <div class="footer-content-comment">
                                SampleSiteとは最新のテクノロジーをコメントを<br />
                                通じて知識共有するサイトです
                                <div class="sns-icon">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="category">
                            <div class="footer-content-title">カテゴリー</div>
                            <div class="footer-content-comment">
                                <ul>
                                    <li><a href="">&gt; Top</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="menu">
                            <div class="footer-content-title">メニュー</div>
                            <div class="footer-content-comment">
                                <ul>
                                    <li><a href="">&gt; 運営会社</a></li>
                                    <li><a href="">&gt; 利用規約</a></li>
                                    <li><a href="">&gt; プライバシーポリシー</a></li>
                                    <li><a href="">&gt; ガイドライン</a></li>
                                    <li><a href="">&gt; お問い合わせ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <p>Copyright© 2019 Sample Example All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>

    <p id="page-top"><a href="">^</a></p>
    <div id="stalker"></div>
    <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>