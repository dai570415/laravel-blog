<?php

Auth::routes();

Route::get('/', function () {
    return view('index');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/user', 'UserController@index')->name('user.index')->middleware('auth');
Route::get('/user/userEdit', 'UserController@userEdit')->name('user.userEdit')->middleware('auth');
Route::post('/user/userEdit', 'UserController@userUpdate')->name('user.userUpdate')->middleware('auth');

// Route::resource('/post', 'PostController')->middleware('auth');
Route::get('post', 'PostController@index')->name('post.index'); // 一覧
Route::post('post', 'PostController@store')->name('post.store')->middleware('auth'); // 保存
Route::get('post/create', 'PostController@create')->name('post.create')->middleware('auth'); // 作成
Route::get('post/{post_id}', 'PostController@show')->name('post.show'); // 表示
Route::get('post/edit/{post_id}', 'PostController@edit')->name('post.edit')->middleware('auth'); // 編集
Route::put('post/{post_id}', 'PostController@update')->name('post.update')->middleware('auth'); // 更新
Route::delete('post/{post_id}', 'PostController@destroy')->name('post.destroy')->middleware('auth'); // 削除