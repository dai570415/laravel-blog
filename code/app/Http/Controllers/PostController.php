<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $authUser = Auth::user();
        $users = User::all();
        $items = Post::with('user')->get();
        $pickupItems =  Post::inRandomOrder()->limit(4)->get();

        $params = [
            'authUser' => $authUser,
            'users' => $users,
            'items' => $items,
            'pickupItems' => $pickupItems,
        ];
        return view('post.index', $params);
    }

    public function store(Request $request)
    {
        $post = new Post;
        $form = $request->all();

        $rules = [
            'user_id' => 'integer|required',
            'title' => 'required',
            'message' => 'required',
        ];
        $message = [
            'user_id.integer' => 'System Error',
            'user_id.required' => 'System Error',
            'title.required'=> 'タイトルが入力されていません',
            'message.required'=> 'メッセージが入力されていません',
        ];
        $validator = Validator::make($form, $rules, $message);

        if($validator->fails()){
            return redirect(route('post.index'))
                ->withErrors($validator)
                ->withInput();
        }

        $uploadfile = $request->file('image');

        if(!empty($uploadfile)){
            $imagename = $request->file('image')->hashName();
            $request->file('image')->storeAs('public/post', $imagename);

            $param = [
                'user_id'=>$request->user_id,
                'title'=>$request->title,
                'message'=>$request->message,
                'category'=>$request->category,
                'image'=>$imagename,
            ];
        } else {
            $param = [
                'user_id'=>$request->user_id,
                'title'=>$request->title,
                'message'=>$request->message,
                'category'=>$request->category,
            ];
        }
        
        Post::create($param);
        return redirect(route('post.index'))->with('success', '保存しました。');
    }

    public function show($id)
    {
        $authUser = Auth::user();
        $users = User::all();
        $item = Post::find($id);
        $params = [
            'authUser' => $authUser,
            'users' => $users,
            'item' => $item,
        ];
        return view('post.show', $params);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $form = $request->all();

        $uploadfile = $request->file('image');

        if(!empty($uploadfile)){
            $imagename = $request->file('image')->hashName();
            $request->file('image')->storeAs('public/post', $imagename);

            $param = [
                'user_id'=>$request->user_id,
                'title'=>$request->title,
                'message'=>$request->message,
                'category'=>$request->category,
                'image'=>$imagename,
            ];
        } else {
            $param = [
                'user_id'=>$request->user_id,
                'title'=>$request->title,
                'message'=>$request->message,
                'category'=>$request->category,
            ];
        }

        Post::where('id',$post->id)->update($param);
        return redirect(route('post.index'))->with('success', '保存しました。');
    }

    public function destroy($id)
    {
        $items = Post::find($id)->delete();
        return redirect(route('post.index'));
    }
}